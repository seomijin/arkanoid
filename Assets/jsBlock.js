﻿#pragma strict

var hit = 1;
var sound     : AudioClip;		// 공과 충돌음
var explosion : Transform;		// 폭파 불꽃

//----------------------------
// 충돌후의 처리
//----------------------------
function SetCollision(flag : boolean) {
	AudioSource.PlayClipAtPoint(sound, transform.position);

	if (jsGameManager.state != STATE.DEMO) 
		hit--;
	if (flag == true) 
		hit = -1;
	
	if (hit >= 0) {
		jsGameManager.state = STATE.HIT; 
		
		var color = transform.renderer.material.color;		// 현재 컬러 보존
		transform.renderer.material.color = Color.red;		// 블록 색깔 변경
		
		yield WaitForSeconds(0.2);
		transform.renderer.material.color = color;			// 색깔 복원
	}
	else {
		// 폭파 불꽃
		Instantiate(explosion, transform.position, Quaternion.identity);
		
		jsGameManager.state = STATE.DESTROY; 
		jsGameManager.blockNum = int.Parse(transform.tag.Substring(5, 1));
		
		jsGameManager.blockPos = transform.position;
		Destroy(gameObject);
	}
}
