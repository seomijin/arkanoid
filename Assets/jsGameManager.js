﻿#pragma strict


var paddle : Transform;			
var ball   : Transform;			
var bonus  : Transform; 		
var speedBall : Transform;		


var txtScore	 : GUIText;
var txtStage 	 : GUIText;
var txtTryAgain  : GUIText;



static var ballCnt = 3;				
static var bonusNum : int;			

private var stageCnt = 5;			
private var stageNum = 1;			
private var score = 0;				


enum STATE {STAGE, RESET, HIT, DESTROY, OUT, BONUS, IDLE, READY, DEMO};

static var state : STATE = STATE.STAGE;
static var blockNum : int;
static var blockPos : Vector3;


function Update () {
	 	
	switch (state) {
		case STATE.STAGE :		 
			MakeStage();
			break;
		case STATE.RESET :		
			ResetPosition();
			break;
		case STATE.HIT :	
			SetHit();
			break;
		case STATE.DESTROY :
			SetDestroy();
			break;
		case STATE.OUT :		
			SetOut();
			break;
		case STATE.BONUS :	
			ProcessBonus();
			break;
	}		
	
	
}


function MakeStage() {
	var n = stageNum % stageCnt;	
	if (n == 0) 
		n = stageCnt;

	
	var floor : GameObject = GameObject.Find("바닥");
	floor.renderer.material.mainTexture = Resources.Load("space" + n, Texture2D);  

	 
	var px = -3.0;			
	var pz = 0.0;			
	var w = 1.0;			
	var h = 0.5;
	
	var tmp = jsStage.Stage[n - 1];			
	
	for (var i = 0; i < 11; i++) {				
		var s : String = tmp[i]; 			
		var x = px;							
		
		for (var j = 0; j < s.Length; j++) {	
			var ch = s.Substring(j, 1);			
			if (ch == "." || ch == " ") {
				x += w;							
				continue;			
			}	
			if (ch == "-") {					
				x += w / 2;						
				continue;
			}	
			
			
			var block : GameObject = Instantiate(Resources.Load("Prefabs/pfBlock" + ch, GameObject));
			block.transform.position = Vector3(x, 0, pz);
			x += w;
		}; 
		
		pz -= h;				
	} 
	
	ResetPosition();			
}
  
  
  
function ResetPosition() {
	
	var padLen =  1.85 - stageNum * 0.15;
	if (padLen < 1.0)
		padLen = 1.0;


	paddle.transform.position.x = 0;
	paddle.transform.localScale = Vector3(padLen, 0.25, 0.25);
	
	ball.transform.position = Vector3(0, 0, paddle.transform.position.z + 0.32);

	var arAngles = [-30, -45, -60, 60, 45, 30];
	ball.transform.rotation.eulerAngles.y = arAngles[ Random.Range(0, 6) ];

	jsBall.speed = 5.0 + stageNum * 0.5;
	
	state = STATE.READY;
}



function SetOut() {
	state = STATE.RESET;
	jsBall.undeadTime = 0;		
	jsPaddle.laserTime = 0;		

	ballCnt--;
	if (ballCnt < 0) { 
		ball.transform.position.z = -10;
		state = STATE.DEMO;
		ShowMessage();			
	}	
}



function SetHit() {
	score += 100;
	if (jsBall.speed < 10)
		jsBall.speed += 0.05;		
	state = STATE.IDLE;
}




function SetDestroy() {
	state = STATE.IDLE;
	
	score += (500 * blockNum);
	if (jsBall.speed < 10)
		jsBall.speed += 0.05;		
		

	if (GetBlockCount() == 0) {
		stageNum++;
		ClearStage();			
		state = STATE.STAGE;
		return;
	}	
	
	MakeBonus();
}	




function GetBlockCount() {
	var cnt = 0;
	for (var i = 1; i <= 4; i++) {
		cnt += GameObject.FindGameObjectsWithTag("BLOCK" + i).Length;
	}
	return cnt;
}



function ClearStage() {

	var balls = GameObject.FindGameObjectsWithTag("BALL9");
	for (var obj in balls)
		Destroy(obj);


	for (var i = 1; i <= 4; i++) {
		var blocks = GameObject.FindGameObjectsWithTag("BLOCK" + i);
		for (obj in blocks)
			Destroy(obj);
	}
}




function MakeBonus() {
	var n = Random.Range(0, 100);
	if (n <= 95) return;
	
	var colors = [Color.yellow, Color.green, Color.red, Color.cyan, Color.black];
	n = Random.Range(1, 6);		
	
	try {
		var obj = Instantiate(bonus, blockPos, Quaternion.identity);
		obj.Find("Sphere").renderer.material.color = colors[n - 1];
		obj.Find("Sphere").tag = "BONUS" + n;
	} catch (err) {
		
	}	
}




function ProcessBonus() {
	switch (bonusNum) {
		case 1 :			
			if (ballCnt < 5)
				ballCnt++;
			break;
		case 2 :			 
			paddle.transform.localScale.x += 0.5;
			if (paddle.transform.localScale.x > 2.2)
				paddle.transform.localScale.x = 2.2;
			break;
		case 3 :		 
			jsBall.undeadTime = 5;
			break;
		case 4 :			
			jsPaddle.laserTime = 10;
			break;
		case 5 :			 
			MakeSpeedBall();
			break;
	}
		
	state = STATE.IDLE;
}			
			



function MakeSpeedBall() {
	var cnt = Random.Range(6, 11);		
	var pos : Vector3 = paddle.transform.position;
	pos.z += 1f;

	for (var i = 1; i <= cnt; i++) {
		var obj : Transform = Instantiate(speedBall, pos, Quaternion.identity);
		obj.renderer.material.color = Color.black;
		obj.SendMessage("SetSpeed", SendMessageOptions.DontRequireReceiver);
		
		yield WaitForSeconds(0.2);		
	}
}




function OnGUI() {
	var w = Screen.width;
	var h = Screen.height;
	
	
	for (var i = 1; i <= ballCnt; i++)	{
		var x = 22 * i - 17;
		var y = h - 15;
		GUI.DrawTexture(Rect(x, y, 20, 10), Resources.Load("imgPaddle", Texture2D));
	}
	

	txtStage.text = "Stage : " + stageNum;
	txtScore.text = "Score : " + score.ToString("n0");;			
}




function ShowMessage() {
	txtTryAgain.text = "Try Again? (y/n)";
	do {
		var isYes = Input.GetKeyUp(KeyCode.Y);
		var isNo = Input.GetKeyUp(KeyCode.N);

		yield 0;
	} while (!isYes && !isNo);
	txtTryAgain.text = "";
	

	if (isYes) {
		score = 0;
		ballCnt = 3;
		stageNum = 1;
		ClearStage();
		MakeStage();
	}
	else { 
		Application.LoadLevel("GameTitle");
	}	
}
